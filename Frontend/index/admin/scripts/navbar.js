document.write(`
<aside class="main-sidebar sidebar-dark-primary elevation-4">
<!-- Brand Logo -->
<a href="index3.html" class="brand-link">
  <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
  <span class="brand-text font-weight-light">Admin Bientl</span>
</a>

<!-- Sidebar -->
<div class="sidebar">
  <!-- Sidebar user (optional) -->
  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <div class="image">
      <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
    </div>
    <div class="info">
      <a href="#" class="d-block">Trinh Long Bien</a>
    </div>
  </div>

  <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <li class="nav-item">
        <a href="productAdmin.html" class="nav-link">
          <i class="fa-solid fa-table mr-2"></i>
          <p>
            Quản lý sản phẩm
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="userAdmin.html" class="nav-link ">
          <i class="fa-solid fa-table mr-2"></i>
          <p>
            Quản lý người dùng
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="orderAdmin.html" class="nav-link ">
          <i class="fa-solid fa-table mr-2"></i>
          <p>
            Quản lý đơn hàng
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="chart.html" class="nav-link  ">
          <i class="fa-solid fa-chart-simple mr-2"></i>
          <p>
            Thống kê
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="#" class="nav-link ">
          <i class="fa-regular fa-user mr-2"></i>
          <p>
            Tài khoản
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a id="logout" class="nav-link ">
          <i class="fa-regular fa-user mr-2"></i>
          <p>
            Đăng xuất
          </p>
          <script>

          </script>
        </a>
      </li>

    </ul>
  </nav>
  <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>
`)

var path = window.location.pathname;
var page = path.split("/").pop();

 var navLink = document.getElementsByClassName("nav-link");
 console.log(navLink);


