"use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gURL = "http://localhost:8080/products";

    const gID_COL = 0;
    const gNAME_COL = 1;
    const gDESCRIPTION_COL = 2;
    const gSTATUS_COL = 3;
    const gACTION_COL = 4;

    const gPROPERTY = ["id","name","description","status","action"];
    
    var gId= 0;
    var gStt = 1;
    // định nghĩa table  
    var gDataTable = $("#data-table").DataTable( {
      autoWidth:false,
      columns : [
        { data : gPROPERTY[gID_COL]},
        { data : gPROPERTY[gNAME_COL]},
        { data : gPROPERTY[gDESCRIPTION_COL]},
        { data : gPROPERTY[gSTATUS_COL]},
        { data : gPROPERTY[gACTION_COL]}
      ],
      columnDefs: [ 
      {
          targets: gACTION_COL,
          className:"text-center",
          defaultContent: 
          `<i style='cursor:pointer' data-toggle="tooltip" data-placement="top" title="Cập nhật" class='fas fa-edit icon-detail mr-2'></i>
          <i  style='cursor:pointer' data-toggle="tooltip" data-placement="top" title="Xóa" class="fas fa-trash-alt icon-delete"></i>`
      },
      {
        targets: gSTATUS_COL,
        className:"text-center",
        render: function(data){
          var status = '<span class="badge badge-success badge-pill">'+data+'</span>';
          if(data == "Inactive"){
            status = '<span class="badge badge-danger badge-pill">'+data+'</span>';
          }
         return status;
        }
      }


    ]
    });
    
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    getAllProduct();

    // Hàm gọi API Get all
    function getAllProduct() {
        $.ajax({
          url: gURL,
          type: "GET",
          dataType: 'json',
          success: function(responseObject){
            loadDataToTable(responseObject);
          },
          error: function(error){
            console.assert(error.responseText);
          }
        });
    }

    // load data to table
    function loadDataToTable(paramResponseObject) {
      gDataTable.clear();
      gDataTable.rows.add(paramResponseObject);
      gDataTable.draw();
    }

    $('label').addClass('col-form-label font-weight-normal');
    
    // CREATE
    $('#create-subject').on('click', function(){
      $('#create-modal').modal('show');
    });

    $('#btn-create').on('click', function(){
        onBtnCreate();
    });

    function onBtnCreate(){
      var formData = new FormData();
      
      var myFile = $('#file-upload-input').prop('files');
      formData.append("file",myFile[0]);

      var productPhoto = "";
      if(myFile[0] != undefined){
        productPhoto = myFile[0].name;
      }

      var productData = {
        name: $("#inp-name").val().trim(),
        price: $("#inp-price").val().trim(),
        photo: productPhoto,
        description: $("#inp-description").val().trim(),
        categoryId: $("#select-cagotery").val()
      }

      var vCheck = validateData(productData);
      if(vCheck == true) {
          $.ajax({
            url: "http://localhost:8080/products/create?categoryId="+ productData.categoryId,
            type: "POST",
            async: true,
            data: JSON.stringify(productData) ,
            contentType: "application/json",
            success: function (res) {
                uploadFile(formData);
            },
            error: function (err) {
                console.error(err);
            }
          });
      } 
    }

    function uploadFile(paramObj){
      //use $.ajax() to upload file
      $.ajax({
          url: "http://localhost:8080/products/image",
          type: "POST",
          data: paramObj,
          enctype: 'multipart/form-data',
          processData: false,
          contentType: false,
          cache: false,
          success: function (res) {
              alert("Tạo mới thành công!")
          },
          error: function (err) {
              console.error(err);
          }
      });
    }

    // UPDATE
    $("#data-table").on("click", ".icon-detail", function() {
        onIconDetailClick(this); 
    });
    $('#btn-update').on('click',function(){
        onBtnUpdate();
    });

    // Hàm xử lý chi tiết sp
    function onIconDetailClick(paramElemet) {
        var vRowSelected = $(paramElemet).parents('tr');
        var vDatatableRow = gDataTable.row(vRowSelected); 
        var vData = vDatatableRow.data();
        // Hiện modal chi tiết đơn hàng
        gId = vData.id;
        $('#update-modal').modal('show');
        console.log(vData);
        $("#inp-name-update").val(vData.name);
        $("#inp-price-update").val(vData.price);
        $("#inp-description-update").val(vData.description);
        $("#select-cagotery-update").val(vData.cateId);
        if(vData.status == "Active"){
          $("#inp-status").prop('checked', true);
        }
        if(vData.status == "Inactive"){
          $("#inp-status").prop('checked', false);
        }
    }

    // Hàm xử lý nút xác nhận cập nhật 
    function onBtnUpdate(){
        var vProductRequest ={
          id: gId,
          name: $("#inp-name-update").val().trim(),
          price:$("#inp-price-update").val().trim(),
          description:$("#inp-description-update").val().trim(),
          status: "Active",
          cateId:$("#select-cagotery-update").val().trim(),
        }
        if($("#inp-status").prop("checked") == false){
          vProductRequest.status = "Inactive"
        }

        if(validateData(vProductRequest) == true){
          console.log(vProductRequest);
          callApiUpdate(vProductRequest);
        }
      }

    // Hàm gọi API Update 
    function callApiUpdate(paramObj){
      $.ajax({
        url:`http://localhost:8080/products/update/${paramObj.id}?categoryId=${paramObj.cateId}`,
        type:"PUT",
        contentType:"application/json",
        data: JSON.stringify(paramObj),
        success: function(){
          $('#update-modal').modal('hide');
          alertToast("success",`Cập nhật thành công`);
          getAllProduct();
        },
        error: function(){
          alert('Cập nhật thất bại!')
        }
      })
    }

    // DELETE
    $("#data-table").on("click", ".icon-delete", function() {
        onIconDeleteClick(this); 
    });


    // Hàm xử lý nút delete
    function onIconDeleteClick(paramElemet) {
        var vRowSelected = $(paramElemet).parents('tr');
        var vDatatableRow = gDataTable.row(vRowSelected); 
        var vData = vDatatableRow.data();

        $.ajax({
          url:`http://localhost:8080/products/delete/${vData.id}`,
          type:"PUT",
          contentType:"application/json",
          success: function(){
            $('#update-modal').modal('hide');
            alertToast("error",`Đã xóa sản phẩm`);
            getAllProduct();
          },
          error: function(){
            alert('Xóa thất bại!')
          }
        })
       
    }
    
    // Hàm kiểm tra thông tin tạo mới
    function validateData(paramDepartment){
        if(paramDepartment.name == ""){
            alert('Tên sản phẩm không được để trống!');
            return false;
        }
        if(Number(paramDepartment.price) <= 0){
            alert('Giá sản phẩm không hợp lệ');
            return false;
        }
        if(paramDepartment.categoryId == ""){
            alert('Hãy chọn danh mục');
            return false;
        }
        if(paramDepartment.photo == ""){
          alert('Hãy chọn ảnh sản phẩm');
          return false;
        }
        return true;
    }

    // Hàm tạo thông báo toast
    function alertToast(paramIcon, paramTittle){
        var Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000
        });                
        Toast.fire({
            icon: paramIcon,
            title: paramTittle
        });
    }

