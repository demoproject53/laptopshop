"use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gURL = "http://localhost:8080/orders";

    const gORDERCODE_COL = 0;
    const gNAME_COL = 1;
    const gCONTACT_COL = 2;
    const gADDRESS_COL = 3;
    const gDATE_COL = 4;
    const gTOTAL_COL = 5;
    const gSTATUS_COL = 6;
    const gACTION_COL = 7;


    const gPROPERTY = ["orderCode","name","contact","address","createDate","totalPrice","status","action"];
    
    var gId= 0;
    // định nghĩa table  
    var gDataTable = $("#data-table").DataTable( {
      autoWidth:false,
      columns : [
        { data : gPROPERTY[gORDERCODE_COL]},
        { data : gPROPERTY[gNAME_COL]},
        { data : gPROPERTY[gCONTACT_COL]},
        { data : gPROPERTY[gADDRESS_COL]},
        { data : gPROPERTY[gDATE_COL]},
        { data : gPROPERTY[gTOTAL_COL]},
        { data : gPROPERTY[gSTATUS_COL]},
        { data : gPROPERTY[gACTION_COL]}

      ],
      columnDefs: [ 
      {
          targets: gACTION_COL,
          className:"text-center",
          defaultContent: 
          `<i style='cursor:pointer' data-toggle="tooltip" data-placement="top" title="Cập nhật" class='fas fa-edit icon-detail mr-2'></i>`
      },
      {
        targets: gSTATUS_COL,
        className:"text-center",
        render: function(data){
          var status = '<span class="badge badge-info badge-pill">'+data+'</span>';
          if(data == "Confirm"){
            status = '<span class="badge badge-success badge-pill">'+data+'</span>';
          }
          else if(data == "Cancel"){
            status = '<span class="badge badge-danger badge-pill">'+data+'</span>';
          }
         return status;
        }
      },
      {
        targets: gTOTAL_COL,
        className:"text-right",
        render: function(data){
          return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }
      }
    ]
    });
    
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    getAllProduct();

    // Hàm gọi API Get all
    function getAllProduct() {
        $.ajax({
          url: gURL,
          type: "GET",
          dataType: 'json',
          success: function(responseObject){
            loadDataToTable(responseObject);
          },
          error: function(error){
            console.assert(error.responseText);
          }
        });
    }

    // load data to table
    function loadDataToTable(paramResponseObject) {
      gDataTable.clear();
      gDataTable.rows.add(paramResponseObject);
      gDataTable.draw();
    }


    // UPDATE
    $("#data-table").on("click", ".icon-detail", function() {
        var vRowSelected = $(this).parents('tr');
        var vDatatableRow = gDataTable.row(vRowSelected); 
        var vData = vDatatableRow.data();
        window.location.href = "view-order.html?order="+ vData.orderCode;
    });









