"use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gURL = "http://localhost:8080/users";

    const gNAME_COL = 0;
    const gUSERNAME_COL = 1;
    const gPHONE_COL = 2;
    const gEMAIL_COL = 3;
    const gACTION_COL = 4;

    const gPROPERTY = ["fullName","userName","phoneNumber","email","action"];
    
    var gId= 0;
    var gStt = 1;
    // định nghĩa table  
    var gDataTable = $("#data-table").DataTable( {
      autoWidth:false,
      columns : [
        { data : gPROPERTY[gNAME_COL]},
        { data : gPROPERTY[gUSERNAME_COL]},
        { data : gPROPERTY[gPHONE_COL]},
        { data : gPROPERTY[gEMAIL_COL]},
        { data : gPROPERTY[gACTION_COL]}
      ],
      columnDefs: [ 
      {
          targets: gACTION_COL,
          className:"text-center",
          defaultContent: 
          `<i style='cursor:pointer' data-toggle="tooltip" data-placement="top" title="Cập nhật" class='fas fa-edit icon-detail mr-2'></i>
          <i  style='cursor:pointer' data-toggle="tooltip" data-placement="top" title="Xóa" class="fas fa-trash-alt icon-delete"></i>`
      }
    ]
    });
    
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    getAllProduct();

    // Hàm gọi API Get all
    function getAllProduct() {
        $.ajax({
          url: gURL,
          type: "GET",
          dataType: 'json',
          success: function(responseObject){
            loadDataToTable(responseObject);
          },
          error: function(error){
            console.assert(error.responseText);
          }
        });
    }

    // load data to table
    function loadDataToTable(paramResponseObject) {
      gDataTable.clear();
      gDataTable.rows.add(paramResponseObject);
      gDataTable.draw();
    }

    $('label').addClass('col-form-label font-weight-normal');
    
    // CREATE
    $('#create-subject').on('click', function(){
      $('#create-modal').modal('show');
    });

    $('#btn-create').on('click', function(){
        onBtnCreate();
    });

    function onBtnCreate(){
      var userData = {
        userName: $("#inp-user-name").val().trim(),
        password: $("#inp-password").val().trim(),
        email: $("#inp-email").val().trim(),
        fullName: $("#inp-full-name").val().trim(),
        phoneNumber: $("#inp-phone-number").val().trim(),
        address: $("#inp-address").val().trim(),
        admin: $("input[name='authority1']:checked").val()
      }
      var vCheck = validateData(userData);
      if(vCheck == true){
        createUser(userData);
      }

    }

    function validateData(paramObj){
        if(paramObj.userName == ""){
          alert("Tên đăng nhập không được để trống!");
          return false;
        }
        else if(paramObj.password == ""){
          alert("Mật khẩu không được để trống!");
          return false;
        }
        else if(paramObj.email == ""){
          alert("Email không được để trống!");
          return false;
        }
        else if(paramObj.fullName == ""){
          alert("Họ tên không được để trống!");
          return false;
        }
        else if(paramObj.phoneNumber == ""){
          alert("Số điện thoại không được để trống!");
          return false;
        }
        else if(paramObj.address == ""){
          alert("Địa chỉ không được để trống!");
          return false;
        }
      return true;
    }

    function createUser(paramObj){
      $.ajax({
        url:"http://localhost:8080/users",
        type:"POST",
        contentType: "application/json",	
        data: JSON.stringify(paramObj) ,		
        success: function(paramRes){
          alertToast("success",`Tạo mới thành công`);
          $('#create-modal').modal('hide');
          getAllProduct();
        },
        error:function(){
          alert("fail!");
        }
      })
    }


    // UPDATE
    $("#data-table").on("click", ".icon-detail", function() {
        onIconDetailClick(this); 
    });
    $('#btn-update').on('click',function(){
        onBtnUpdate();
    });

    // Hàm xử lý load chi tiết 
    function onIconDetailClick(paramElemet) {
        var vRowSelected = $(paramElemet).parents('tr');
        var vDatatableRow = gDataTable.row(vRowSelected); 
        var vData = vDatatableRow.data();
        // Hiện modal chi tiết 
        gId = vData.id;
        $('#update-modal').modal('show');
        console.log(vData);
        $("#inp-user-name-update").val(vData.userName);
        $("#inp-password-update").val(vData.password);
        $("#inp-email-update").val(vData.email);
        $("#inp-full-name-update").val(vData.fullName);
        $("#inp-phone-number-update").val(vData.phoneNumber);
        $("#inp-address-update").val(vData.address);

        var $radios = $("input[name='authority2'");
        if(vData.admin === true) {
            $radios.filter('[value=true]').prop('checked', true);
        }
        else {
          $radios.filter('[value=false]').prop('checked', true);
        }

    }

    // Hàm xử lý nút xác nhận cập nhật 
    function onBtnUpdate(){
      var userData = {
        userName: $("#inp-user-name-update").val().trim(),
        password: $("#inp-password-update").val().trim(),
        email: $("#inp-email-update").val().trim(),
        fullName: $("#inp-full-name-update").val().trim(),
        phoneNumber: $("#inp-phone-number-update").val().trim(),
        address: $("#inp-address-update").val().trim(),
        admin: $("input[name='authority2']:checked").val()
      }

        if(validateData(userData) == true){
          callApiUpdate(userData);
        }
      }

    // Hàm gọi API Update 
    function callApiUpdate(paramObj){
      $.ajax({
        url:`http://localhost:8080/users/${gId}`,
        type:"PUT",
        contentType:"application/json",
        data: JSON.stringify(paramObj),
        success: function(){
          $('#update-modal').modal('hide');
          alertToast("success",`Cập nhật thành công`);
          getAllProduct();
        },
        error: function(){
          alert('Cập nhật thất bại!')
        }
      })
    }

    // Hàm tạo thông báo toast
    function alertToast(paramIcon, paramTittle){
        var Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000
        });                
        Toast.fire({
            icon: paramIcon,
            title: paramTittle
        });
    }

