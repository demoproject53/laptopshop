document.write(`
    <!-- TOP HEADER -->
    <div class="container-fluid py-2" style="background-color: #333;">
        <div class="container text-white">
    <div class="row">
    <div class="col-sm-6">
    <span class="mr-3"><i class="fa fa-phone text-danger"></i> +021-95-51-84</span>
    <span class="mr-3"><i class="fa-solid fa-envelope text-danger"></i></i> bientl@email.com</span>
    <span class="mr-3"><i class="fa fa-map-marker text-danger"></i> Nam Từ Liêm</span>
    </div>
    <div id="user-login" class="col-sm-6  d-flex justify-content-end" >
    <a href="login.html" class="text-white"><i class="fa-regular fa-user text-danger px-2"></i>Tài khoản</a> 
    </div>
    </div>
        </div>
    </div>

    <!-- MAIN HEADER -->
    <div  class="container-fluid sticky-top main-header"  >
    <div class="container">
    <div class="row">
    <!-- LOGO -->
    <div class="col-sm-3">
    <img src="../../images/logon.png" alt="" style="height: 75px;">
    </div>

    <!-- SEARCH BAR -->
    <div class="col-sm-6 mt-3">
    <div class="input-group mb-2 mr-sm-2">
        <input type="text" class="form-control" id="inp-product-search" placeholder="Nhập tên sản phẩm . . ." style="border-radius: 20px 0 0 20px;">
        <div class="input-group-prepend">
        <button id="btn-search" class="btn btn-danger" style="border-radius:0 20px 20px 0;">Tìm kiếm</button>
        </div>
    </div>
    </div>

    <!-- CART -->
    <div class="col-sm-3 text-white mt-4 text-center">
        <div id="btn-show-cart">
        <i class="fa fa-shopping-cart fa-2xl" style="cursor: pointer;" ></i>
        <span id="cart-quantity"></span>
        </div>
    </div>
    </div>
    </div>
    </div>

    <!-- NAVIGATION -->
    <nav class="container-fluid bg-light py-1" >
    <div class="container py-2">
    <div class="row">
    <div id="navbar" class="col-sm-12 d-flex justify-content-around">
        <a href="home.html" class="nav-item">TRANG CHỦ</a>
        <a href="#" class="nav-item">KHUYẾN MÃI</a>
        <a href="#" class="nav-item">TRẢ GÓP</a>
        <a href="#" class="nav-item">LIÊN HỆ</a>
    </div>
    </div>
    </div>
    </nav>

`)

