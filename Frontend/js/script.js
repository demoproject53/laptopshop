
/****************** CART *****************************/
var cartItemSession = JSON.parse(sessionStorage.getItem("CartItems"));
  if(cartItemSession == null){
    cartItemSession = [];
  }

var gTotalCartPrice = 0;

// Sự kiện hiện giỏ hàng
$("#btn-show-cart").on("click",function(){
    $("#modal-cart").modal("show");
    showCartItems(cartItemSession);
})

//Sự kiện thay đổi số lượng
$("#cart-table").on("click",".inp-qty",function(){
    onChangeQtyProduct(this);
})

function addToCart(paramID){
    var item = {
      productId: paramID,
      qty:1
    }
    // Lấy các thông tin sản phẩm
    for (const product of allProductList) {
      if(product.id == item.productId){
        item.photo = product.photo;
        item.name = product.name;
        item.price = product.price;
        item.totalPrice = item.price*1;
      }
    }

    if(cartItemSession.length == 0){
      cartItemSession.push(item);
      addItemToSession(cartItemSession)
    } 
    else {
      var vCheck = false // check xem sp đã có trong giỏ hàng chưa?
      for(var i = 0; i < cartItemSession.length; i++ ){
        if(cartItemSession[i].productId == paramID){
          cartItemSession[i].qty ++;
          cartItemSession[i].totalPrice = cartItemSession[i].price * cartItemSession[i].qty;
          vCheck = true;
        }
      }
      if(vCheck == true){
        addItemToSession(cartItemSession)
      }
      else {
        cartItemSession.push(item);
        addItemToSession(cartItemSession)
      }
    }
  }

function addItemToSession(paramObj){
  sessionStorage.setItem("CartItems", JSON.stringify(paramObj));
  alert("Sản phẩm đã được thêm vào giỏ hàng");
  showCartQuantity();
}

// Load item vào giỏ hàng
function showCartItems(paramObj){
  // Danh sách giỏ hàng
  var cartTable = $("#cart-table");
  cartTable.empty();
      for(var i=0; i < paramObj.length; i++ ){
        var newRow = $("<tr>");
        //Gán dữ liệu cho row
        newRow.data({
          "id":paramObj[i].productId
        })
        //thêm các ô cho row

        var imgCol = $("<td>").appendTo(newRow);
        $("<img>").attr({"src":"../../images/laptops/"+paramObj[i].photo,"width":"60px"}).appendTo(imgCol) ;
        
        $("<td>").html(paramObj[i].name).appendTo(newRow);
        $("<td>").html(paramObj[i].price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")).appendTo(newRow);
          
        var productQtyCol = $("<td>").appendTo(newRow);
        $("<input>").attr({"type":"number", "style": "width:40px;","value":paramObj[i].qty,"class":"inp-qty","min":"0"}).appendTo(productQtyCol);

        $("<td>").html(paramObj[i].totalPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")).appendTo(newRow);
          
        newRow.appendTo(cartTable);
      }
}

// Hiển thị số lượng sản phẩm
showCartQuantity();
function showCartQuantity(){
  var cartQty = 0;
  var cartPrice = 0;
  for (const iterator of cartItemSession) {
    cartQty += iterator.qty;
    cartPrice += iterator.totalPrice
  }
  $("#cart-quantity").html(`${cartQty}`);
  $("#total-price").html(`${cartPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")} VND`);
  gTotalCartPrice = cartPrice;
}

// Xử lý sự kiện thay đổi số lượng sản phẩm
function onChangeQtyProduct(paramElemnt){
    var idRowSelected = $(paramElemnt).parents("tr").data("id");
    console.log(idRowSelected);
    var itemQty = Number($(paramElemnt).val());
    if(itemQty == 0){
      for (const [index,item] of cartItemSession.entries()) {
        if(item.productId == idRowSelected) {
          console.log(index);
          cartItemSession.splice(index,1);
        }
      }
    }
    else {
      for (const item of cartItemSession) {
        if(item.productId == idRowSelected) {
          item.qty = itemQty;
          item.totalPrice = item.qty * item.price;
        }
      }
    }
    showCartItems(cartItemSession);
    sessionStorage.setItem("CartItems", JSON.stringify(cartItemSession));
    showCartQuantity();
}

/****************** CART *****************************/

  $("#btn-search").on("click", function(){
    onBtnProductSearch();
  })

  function onBtnProductSearch(){
      var productName = $("#inp-product-search").val().trim();
      $.ajax({
          url:"http://localhost:8080/products/search/"+productName,
          type:"GET",
          success: function(resParam){
              addProductList(createPageItem(1, resParam));
              createPagination(resParam);
          },
          error: function(){
              alert("fail")
          }
      })
  }

/****************** USER *****************************/

  var userDataSession = JSON.parse(sessionStorage.getItem("UserData"));
      if(userDataSession != null){
          $("#user-login").html(`
          <a href="listOrder.html" class="text-white pr-3"><i class="fa-regular fa-user text-danger px-2"></i>Xin chào ${userDataSession.userName}</a>
          <a href="home.html" id="log-out"  class="text-white pl-3" style="border-left:1px solid white">Đăng xuất</a>
          `);

        if(userDataSession.admin == true){
            $(`<a href="../admin/productAdmin.html" class="nav-item">QUẢN LÝ</a>`).appendTo($("#navbar"));
        };
      } 


  $("#log-out").on("click", function(){
      sessionStorage.removeItem("UserData");
  })
