document.write(`
<!-- FOOTER -->
<footer  class="container-fluid footer mt-5"  >
  <div class="container text-secondary">
    <div class="row mt-5">
      <div class="col-sm-3">
        <H5 style="font-weight: 700; color:gainsboro">BIENTL</H5>
        <p>bientl.com là website thương mại điện tử lớn nhất Việt Nam, cung cấp tất cả các nhãn hiệu laptop chính thức tại Việt Nam</p>
        <p class="mr-3"><i class="fa fa-phone text-danger"></i> +021-95-51-84</p>
        <p class="mr-3"><i class="fa-solid fa-envelope text-danger"></i></i> bientl@email.com</p>
        <p class="mr-3"><i class="fa fa-map-marker text-danger"></i> Nam Từ Liêm</p>
      </div>

      <div class="col-sm-3 pl-5">
        <H5 style="font-weight: 700; color:gainsboro">DANH MỤC</H5>
        <p>Ưu đãi</p>
        <p>Laptop</p>
        <p style="margin-top: 20px;">Điện thoại</p>
        <p>Camera</p>
        <p>Phụ kiện</p>
      </div>

      <div class="col-sm-3 pl-5">
        <H5 style="font-weight: 700; color:gainsboro">THÔNG TIN</H5>
        <p>Về chúng tôi</p>
        <p>Liên hệ</p>
        <p style="margin-top: 20px;">Chính sách</p>
        <p>Hoàn trả</p>
        <p>Điều khoản</p>
      </div>

      <div class="col-sm-3 pl-5">
        <H5 style="font-weight: 700; color:gainsboro">DỊCH VỤ</H5>
        <p>Tài khoản</p>
        <p>Giỏ hàng</p>
        <p style="margin-top: 20px;">Đơn hàng</p>
        <p>Trả góp</p>
        <p>Liên hệ</p>
      </div>
    </div>
  </div>
</footer>
`)