-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 15, 2022 at 12:13 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laptop_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_type` varchar(40) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_type`) VALUES
(1, 'Học tập, văn phòng'),
(2, 'Gaming'),
(3, 'Đồ họa kỹ thuật'),
(4, 'Mỏng nhẹ');

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` bigint(20) NOT NULL,
  `qty` bigint(20) NOT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`id`, `name`, `price`, `qty`, `order_id`, `product_id`, `photo`) VALUES
(38, 'Acer Aspire 3 A315', 10190000, 1, 34, 1, 'l1.jpg'),
(39, 'Lenovo Idealpad 3 15ITL6', 9990999, 1, 34, 2, 'l2.jpg'),
(40, 'Lenovo Idealpad 3 15ITL6', 9990999, 1, 35, 2, 'l2.jpg'),
(41, 'Asus ExpertBook P2541F', 9340000, 1, 35, 3, 'l3.jpg'),
(45, 'Acer Aspire 3 A315', 10190000, 1, 37, 1, 'l1.jpg'),
(47, 'Acer Aspire 3 A315', 10190000, 1, 38, 1, 'l1.jpg'),
(48, 'Lenovo Idealpad 3 15ITL6', 9990999, 1, 38, 2, 'l2.jpg'),
(49, 'Asus ExpertBook P2541F', 9340000, 1, 38, 3, 'l3.jpg'),
(50, 'Lenovo Idealpad 3 15ITL6', 9990999, 2, 39, 2, 'l2.jpg'),
(51, 'Surface Pro 7 i5', 25390000, 3, 39, 40, 'l40.jpg'),
(52, 'Acer Aspire 3 A315', 10190000, 1, 40, 1, 'l1.jpg'),
(53, 'Lenovo Idealpad 3 15ITL6', 9990999, 1, 40, 2, 'l2.jpg'),
(54, 'Asus ExpertBook P2541F', 9340000, 1, 40, 3, 'l3.jpg'),
(55, 'Acer Aspire 3 A356', 10190000, 1, 41, 4, 'l4.jpg'),
(56, 'Asus VivoBook X515EA', 10990000, 2, 41, 5, 'l5.jpg'),
(57, 'Asus ExpertBook P2541F', 9340000, 1, 41, 3, 'l3.jpg'),
(58, 'Asus VivoBook X515EA', 10990000, 1, 42, 5, 'l5.jpg'),
(59, 'Lenovo Idealpad 3 15ITL6', 9990999, 1, 42, 2, 'l2.jpg'),
(60, 'Acer Aspire 3 A315', 10190000, 1, 43, 1, 'l1.jpg'),
(61, 'Acer Aspire 3 A356', 10190000, 1, 43, 4, 'l4.jpg'),
(62, 'Lenovo Yoga Slim 7', 27990000, 1, 43, 37, 'l37.jpg'),
(63, 'Acer Aspire 3 A315', 10190000, 1, 44, 1, 'l1.jpg'),
(64, 'Lenovo Idealpad 3 15ITL6', 9990999, 1, 44, 2, 'l2.jpg'),
(65, 'Asus ExpertBook P2541F', 9340000, 1, 44, 3, 'l3.jpg'),
(66, 'HP 240 G8 i3', 9190000, 1, 45, 7, 'l7.jpg'),
(67, 'HP 340s G7 i3', 9190000, 3, 45, 8, 'l8.jpg'),
(68, 'Acer Aspire 3 A315', 10190000, 1, 46, 1, 'l1.jpg'),
(69, 'Acer Aspire 3 A356', 10190000, 1, 46, 4, 'l4.jpg'),
(70, 'Acer Aspire 3 A356', 10190000, 1, 47, 4, 'l4.jpg'),
(71, 'HP 240 G8 i3', 9190000, 1, 47, 7, 'l7.jpg'),
(72, 'Asus VivoBook X515EA', 10990000, 2, 47, 5, 'l5.jpg'),
(73, 'MSI Modern 14 i5', 19990000, 2, 48, 34, 'l34.jpg'),
(74, 'Acer Aspire 3 A315', 10190000, 2, 48, 1, 'l1.jpg'),
(75, 'Acer Aspire 3 A356', 10190000, 1, 48, 4, 'l4.jpg'),
(76, 'Acer Aspire 3 A315', 10190000, 1, 49, 1, 'l1.jpg'),
(77, 'Lenovo Idealpad 4', 9990999, 1, 49, 2, 'l2.jpg'),
(78, 'Lonovo Yoga Duet 7', 34990000, 1, 50, 31, 'l31.jpg'),
(79, 'Acer Aspire 3 A315', 10190000, 2, 50, 1, 'l1.jpg'),
(80, 'Acer Aspire 3 A315', 10190000, 1, 51, 1, 'l1.jpg'),
(81, 'Lenovo Idealpad 4', 9990999, 2, 51, 2, 'l2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(15) DEFAULT NULL,
  `photo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `photo`, `description`, `status`, `category_id`) VALUES
(1, 'Acer Aspire 3 A315', 10190000, 'l1.jpg', 'Acer Aspire 3 A315 - 10190000', 'Active', 2),
(2, 'Lenovo Idealpad 4', 9990999, 'l2.jpg', 'Lenovo Idealpad 3 15ITL6 - 9990999', 'Active', 1),
(3, 'Asus ExpertBook P2541F', 9340000, 'l3.jpg', 'Asus ExpertBook P2541F - 9340000', 'Active', 1),
(4, 'Acer Aspire 3 A356', 10190000, 'l4.jpg', 'Acer Aspire 3 A356 - 10190000', 'Active', 1),
(5, 'Asus VivoBook X515EA', 10990000, 'l5.jpg', 'Asus VivoBook X515EA - 10990000', 'Active', 1),
(6, 'Dell Insprion 15', 12090000, 'l6.jpg', 'Dell Insprion 15 - 12090000', 'Active', 1),
(7, 'HP 240 G8 i3', 9190000, 'l7.jpg', 'HP 240 G8 i3 - 9190000', 'Active', 1),
(8, 'HP 340s G7 i3', 9190000, 'l8.jpg', 'HP 340s G7 i3 - 9190000', 'Active', 1),
(9, 'HP 245 G8 R3', 8290000, 'l9.jpg', 'HP 245 G8 R3 - 8290000', 'Active', 1),
(10, 'HP 14s fq1080AU', 8690000, 'l10.jpg', 'HP 14s fq1080AU - 8690000', 'Active', 1),
(11, 'Asus TUF Gaming', 19190000, 'l11.jpg', 'Asus TUF Gaming - 19190000', 'Active', 2),
(12, 'Acer Aspire 7 Gaming A715', 16640000, 'l12.jpg', 'Acer Aspire 7 Gaming A715 - 16640000', 'Active', 2),
(13, 'Levono Legion 5', 32140000, 'l13.jpg', 'Levono Legion 5 - 32140000', 'Active', 2),
(14, 'HP VICTUS 16', 21590000, 'l14.jpg', 'HP VICTUS 16 - 21590000', 'Active', 2),
(15, 'Acer Nitro 5 Gaming', 20240000, 'l15.jpg', 'Acer Nitro 5 Gaming - 20240000', 'Active', 2),
(16, 'MSI Gaming GF65', 26540000, 'l16.jpg', 'MSI Gaming GF65 - 26540000', 'Active', 2),
(17, 'Dell Gaming G15', 26390000, 'l17.jpg', 'Dell Gaming G15 - 26390000', 'Active', 2),
(18, 'Lenovo Gaming', 24490000, 'l18.jpg', 'Lenovo Gaming - 24490000', 'Active', 2),
(19, 'Asus TUF Gaming i5', 25990000, 'l19.jpg', 'Asus TUF Gaming i5 - 25990000', 'Active', 2),
(20, 'Levono Idealpad Gaming', 22290000, 'l20.jpg', 'Levono Idealpad Gaming - 22290000', 'Active', 2),
(21, 'Lenovo Gaming Legion 5', 28790000, 'l21.jpg', 'Lenovo Gaming Legion 5 - 28790000', 'Active', 3),
(22, 'MacBook Pro M1', 41190000, 'l22.jpg', 'MacBook Pro M1 - 41190000', 'Active', 3),
(23, 'Lenovo IdeaPad Pro 5', 27490000, 'l23.jpg', 'Lenovo IdeaPad Pro 5 - 27490000', 'Active', 3),
(24, 'Asus VivoBook 15 Pro', 30290000, 'l24.jpg', 'Asus VivoBook 15 Pro - 30290000', 'Active', 3),
(25, 'HP Pavilion 15', 23990000, 'l25.jpg', 'HP Pavilion 15 - 23990000', 'Active', 3),
(26, 'Dell Vostro 3400 i7', 25190000, 'l26.jpg', 'Dell Vostro 3400 i7 - 25190000', 'Active', 3),
(27, 'Dell Vostro 5620 i5', 26490000, 'l27.jpg', 'Dell Vostro 5620 i5 - 26490000', 'Active', 3),
(28, 'MacBook Pro 16', 33490000, 'l28.jpg', 'MacBook Pro 16 - 33490000', 'Active', 3),
(29, 'MSI Gaming GF63 Thin', 21140000, 'l29.jpg', 'MSI Gaming GF63 Thin - 21140000', 'Active', 3),
(30, 'MSI Creator Z216', 30990000, 'l30.jpg', 'MSI Creator Z216 - 30990000', 'Active', 3),
(31, 'Lonovo Yoga Duet 7', 34990000, 'l31.jpg', 'Lonovo Yoga Duet 7 - 34990000', 'Active', 4),
(32, 'MacBook Air M2', 29900000, 'l32.jpg', 'MacBook Air M2 - 29900000', 'Active', 4),
(33, 'Asus ZenBook 1135G7', 21290000, 'l33.jpg', 'Asus ZenBook 1135G7 - 21290000', 'Active', 4),
(34, 'MSI Modern 14 i5', 19990000, 'l34.jpg', 'MSI Modern 14 i5 - 19990000', 'Active', 4),
(35, 'LG gram 2022 i7', 27490000, 'l35.jpg', 'LG gram 2022 i7 - 27490000', 'Active', 4),
(36, 'HP Elitebook 630 G9', 21490000, 'l36.jpg', 'HP Elitebook 630 G9 - 21490000', 'Active', 4),
(37, 'Lenovo Yoga Slim 7', 27990000, 'l37.jpg', 'Lenovo Yoga Slim 7 - 27990000', 'Active', 4),
(38, 'Dell XPS 13 9310 i5', 31990000, 'l38.jpg', 'Dell XPS 13 9310 i5 - 31990000', 'Active', 4),
(39, 'MSI Modern 14 B11MOU', 19990000, 'l39.jpg', 'MSI Modern 14 B11MOU - 19990000', 'Active', 4),
(40, 'Surface Pro 7 i5', 25390000, 'l40.jpg', 'Surface Pro 7 i5 - 25390000', 'Active', 4),
(57, 'Chuwy Herobook', 9000000, 'newimg.jpg', 'Chuwy Herobook', 'Active', 2);

-- --------------------------------------------------------

--
-- Table structure for table `t_order`
--

CREATE TABLE `t_order` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `order_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_order`
--

INSERT INTO `t_order` (`id`, `address`, `contact`, `email`, `name`, `total_price`, `user_id`, `order_code`, `create_date`, `status`) VALUES
(34, 'Cầu Giấy, Hà Nội', '09863653', 'trinhbien95@gmail.com', 'Trịnh Long Biên', '20180999', 3, 'ORDER - 1670893353816', '2022-12-13 01:02:33', 'Confirm'),
(35, 'Cầu Giấy, Hà Nội', '09863653', 'trinhbien95@gmail.com', 'Trịnh Long Biên', '19330999', 3, 'ORDER - 1670895351071', '2022-12-13 01:35:51', 'Confirm'),
(37, 'Cầu Giấy, Hà Nội', '09863653', 'trinhbien95@gmail.com', 'Trịnh Long Biên', '16680000', 3, 'ORDER - 1670928789079', '2022-12-13 10:53:09', 'Cancel'),
(38, 'Pháo Đài Láng', '09999999', 'lam@gmail.com', 'Hoàng Lâm', '29520999', 7, 'ORDER - 1671007965015', '2022-12-14 08:52:45', 'Confirm'),
(39, 'Hà nội', '0982772772', 'a@gmail.com', 'Nguyễn Văn A', '96151998', NULL, 'ORDER - 1671073055949', '2022-12-15 02:57:35', 'Cancel'),
(40, 'Cầu Giấy, Hà Nội', '0986365374', 'trinhbien95@gmail.com', 'Trịnh Long Biên', '29520999', 3, 'ORDER - 1671073105502', '2022-12-15 02:58:25', 'Open'),
(41, 'Hoàn Kiếm, Hà Nôi', '0986251729', 'thu@gmail.com', 'Nguyễn Đình Thư', '41510000', 21, 'ORDER - 1671073630655', '2022-12-15 03:07:10', 'Open'),
(42, 'Hoàn Kiếm, Hà Nôi', '0986251729', 'thu@gmail.com', 'Nguyễn Đình Thư', '20980999', 21, 'ORDER - 1671073643549', '2022-12-15 03:07:23', 'Open'),
(43, 'Hoàn Kiếm, Hà Nôi', '0986251729', 'thu@gmail.com', 'Nguyễn Đình Thư', '48370000', 21, 'ORDER - 1671073658717', '2022-12-15 03:07:38', 'Open'),
(44, 'Nam Từ Liêm, Hà Nội', '0928217822', 'cuong@gmail.com', 'Trần Văn Cương', '29520999', 22, 'ORDER - 1671073687277', '2022-12-15 03:08:07', 'Open'),
(45, 'Nam Từ Liêm, Hà Nội', '0928217822', 'cuong@gmail.com', 'Trần Văn Cương', '36760000', 22, 'ORDER - 1671073701891', '2022-12-15 03:08:21', 'Open'),
(46, 'Quận 1, HCM', '0928371283', 'hien@gmail.com', 'Đinh Thị Hiền', '20380000', 23, 'ORDER - 1671073726070', '2022-12-15 03:08:46', 'Open'),
(47, 'Quận 1, HCM', '0928371283', 'hien@gmail.com', 'Đinh Thị Hiền', '41360000', 23, 'ORDER - 1671073738076', '2022-12-15 03:08:58', 'Open'),
(48, 'Hà Nội', '0982662882', 'bien@gmail.com', 'Long Biên', '70550000', NULL, 'ORDER - 1671075621359', '2022-12-15 03:40:21', 'Open'),
(49, 'Cầu Giấy, Hà Nội', '0986365374', 'trinhbien95@gmail.com', 'Trịnh Long Biên', '20180999', 3, 'ORDER - 1671075660454', '2022-12-15 03:41:00', 'Open'),
(50, 'Hà Nội', '0982762612', 'a@gmail.com', 'Nguyễn Văn A', '55370000', NULL, 'ORDER - 1671075954778', '2022-12-15 03:45:54', 'Open'),
(51, 'Cầu Giấy, Hà Nội', '0986365374', 'trinhbien95@gmail.com', 'Trịnh Long Biên', '30171998', 3, 'ORDER - 1671075996328', '2022-12-15 03:46:36', 'Confirm');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_admin` bit(1) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `address`, `email`, `full_name`, `is_admin`, `password`, `phone_number`, `user_name`) VALUES
(3, 'Cầu Giấy, Hà Nội', 'trinhbien95@gmail.com', 'Trịnh Long Biên', b'0', '123', '0986365374', 'anhbien'),
(7, 'Pháo Đài Láng, Hà Nội', 'lam@gmail.com', 'Hoàng Lâm', b'0', 'lam', '0999999999', 'lamhoang'),
(20, 'Công ty', 'admin@gmail.com', 'admin', b'1', '123', '0999182822', 'admin'),
(21, 'Hoàn Kiếm, Hà Nôi', 'thu@gmail.com', 'Nguyễn Đình Thư', b'0', '123', '0986251729', 'thunl'),
(22, 'Nam Từ Liêm, Hà Nội', 'cuong@gmail.com', 'Trần Văn Cương', b'0', '123', '0928217822', 'cuongtv'),
(23, 'Quận 1, HCM', 'hien@gmail.com', 'Đinh Thị Hiền', b'0', '213', '0928371283', 'hiendinh'),
(24, 'Hai Bà Trưng, Hà Nội', 'kien@gmail.com', 'Nguyễn Văn Kiên', b'0', '123456', '0987251623', 'kien'),
(25, 'Hà Nội', 'bien@gmail.com', 'Nguyễn Văn B', b'0', '123', '0972617622', 'Bvl'),
(26, 'Hà Nội', 'kien@gmail.com', 'Nguyễn Nhật Kiên', b'0', '123', '0987251623', 'kentl');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKp2cbw6281041vcx6qkkqv6lmx` (`order_id`),
  ADD KEY `FKc5uhmwioq5kscilyuchp4w49o` (`product_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK1cf90etcu98x1e6n9aks3tel3` (`category_id`);

--
-- Indexes for table `t_order`
--
ALTER TABLE `t_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKn1oo1sx1svg78ibr8u541941o` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `t_order`
--
ALTER TABLE `t_order`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `order_item`
--
ALTER TABLE `order_item`
  ADD CONSTRAINT `FKc5uhmwioq5kscilyuchp4w49o` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `FKp2cbw6281041vcx6qkkqv6lmx` FOREIGN KEY (`order_id`) REFERENCES `t_order` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `FK1cf90etcu98x1e6n9aks3tel3` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Constraints for table `t_order`
--
ALTER TABLE `t_order`
  ADD CONSTRAINT `FKn1oo1sx1svg78ibr8u541941o` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
