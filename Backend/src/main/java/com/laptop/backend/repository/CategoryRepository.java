package com.laptop.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.laptop.backend.entites.Category;

public interface CategoryRepository extends JpaRepository <Category, Long> {
    
}
