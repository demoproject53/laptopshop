package com.laptop.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.laptop.backend.entites.User;

public interface UserRepository extends JpaRepository<User, Long> {
   User findByUserNameAndPassword(String userName, String password);
}
