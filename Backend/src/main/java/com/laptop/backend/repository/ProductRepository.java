package com.laptop.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.laptop.backend.entites.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findByCategoryId(long id);

    @Query(value = "FROM #{#entityName} WHERE name LIKE %?1%")
    List<Product> findByNameLike(String productName);

}
