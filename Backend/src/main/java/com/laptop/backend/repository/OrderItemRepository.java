package com.laptop.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.laptop.backend.entites.OrderItem;

public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {
    
}
