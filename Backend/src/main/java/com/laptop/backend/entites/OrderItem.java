package com.laptop.backend.entites;

import javax.persistence.*;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "order_item")
public class OrderItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JsonBackReference
    private Order order; 

    @ManyToOne
    @JsonIgnore
    private Product product;

    private String name;

    private long qty;

    private long price;

    private String photo;

    @Transient
    private long productId;
   
    public OrderItem() {
    }

    public OrderItem(long id, Order order, Product product, String name, long qty, long price, String photo,
            long productId) {
        this.id = id;
        this.order = order;
        this.product = product;
        this.name = name;
        this.qty = qty;
        this.price = price;
        this.photo = photo;
        this.productId = productId;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getQty() {
        return qty;
    }

    public void setQty(long qty) {
        this.qty = qty;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getProductId() {
        if(productId == 0){
            return product.getId();
        }
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    
}
