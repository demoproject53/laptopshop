package com.laptop.backend.controller;


import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.laptop.backend.entites.Category;
import com.laptop.backend.entites.Product;
import com.laptop.backend.repository.CategoryRepository;
import com.laptop.backend.repository.ProductRepository;

import org.springframework.web.bind.annotation.*;



@RestController
@CrossOrigin
public class ProductController {
    @Autowired
    ProductRepository productRepo;

    @Autowired
    CategoryRepository categoryRepo;

    @GetMapping(value="/products")
    public ResponseEntity<?> getProduct() {
        try {
            List<Product> products = new ArrayList<>();

            productRepo.findAll().forEach(products::add);

            return new ResponseEntity<>(products, HttpStatus.OK);
        } 
        catch (Exception e) { // Xử lý trả kết quả lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value="/products/{id}")
    public ResponseEntity<?> getProductDetail(@PathVariable("id") Long id) {
        try {
            Optional<Product> productFound = productRepo.findById(id);
            if(productFound.isPresent()){
                return new ResponseEntity<>(productFound.get(), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>("Không thấy product này", HttpStatus.INTERNAL_SERVER_ERROR);
            }
            
        } 
        catch (Exception e) { // Xử lý trả kết quả lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value="/products/category/{id}")
    public ResponseEntity<?> getProductByCategoryId(@PathVariable("id") long id) {
        try {
            List<Product> products = new ArrayList<>();

            productRepo.findByCategoryId(id).forEach(products::add);

            return new ResponseEntity<>(products, HttpStatus.OK);
        } 
        catch (Exception e) { // Xử lý trả kết quả lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value="/products/cart/{itemId}")
    public ResponseEntity<?> getProductsCart(@PathVariable ArrayList<Long> itemId) {
        try {
            List<Product> products = new ArrayList<>();

            productRepo.findAllById(itemId).forEach(products::add);

            return new ResponseEntity<>(products, HttpStatus.OK);
        } 
        catch (Exception e) { // Xử lý trả kết quả lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Tìm sản phẩm theo tên
    @GetMapping(value="/products/search/{name}")
    public ResponseEntity<?> getProductByName(@PathVariable("name") String name) {
        try {
            List<Product> products = new ArrayList<>();

            productRepo.findByNameLike(name).forEach(products::add);

            return new ResponseEntity<>(products, HttpStatus.OK);
        } 
        catch (Exception e) { // Xử lý trả kết quả lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping("/products/image")
    public ResponseEntity<?> fileUpload(@RequestParam("file") MultipartFile file) {
        try {
            // upload directory - change it to your own
            String UPLOAD_DIR = "C://Users//THINK//Desktop//LaptopProject//frontend//images//laptops";
    
            // create a path from the file name
            Path path = Paths.get(UPLOAD_DIR, file.getOriginalFilename());
    
            // save the file to `UPLOAD_DIR` make sure you have permission to write
            Files.write(path, file.getBytes());

            return new ResponseEntity<>("Lưu file thành công", HttpStatus.OK);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>("Invalid file format!!", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/products/create")
    public ResponseEntity<?> createNewProduct(@RequestBody Product productData,
                                                    @RequestParam("categoryId") Long categoryId) {
        try {
            Product newProduct =  new Product();

            newProduct.setName(productData.getName());
            newProduct.setPrice(productData.getPrice());
            newProduct.setPhoto(productData.getPhoto());
            newProduct.setDescription(productData.getDescription());
            newProduct.setStatus("Active");

            Optional<Category> categoryFound = categoryRepo.findById(categoryId);
            if(categoryFound.isPresent()){
                newProduct.setCategory(categoryFound.get());
            }
               
            return new ResponseEntity<>(productRepo.save(newProduct), HttpStatus.OK);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>("Invalid file format!!", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/products/update/{id}")
    public ResponseEntity<?> updateProduct(@RequestBody Product productData, @PathVariable("id") Long id,@RequestParam("categoryId") Long categoryId) {
        try {
            Optional<Product> productCheck = productRepo.findById(id);
            if(productCheck.isPresent()){
                Product productFound = productCheck.get();
                productFound.setName(productData.getName());
                productFound.setPrice(productData.getPrice());
                productFound.setDescription(productData.getDescription());
                productFound.setStatus(productData.getStatus());

                Optional<Category> categoryFound = categoryRepo.findById(categoryId);
                if(categoryFound.isPresent()){
                    productFound.setCategory(categoryFound.get());
                }
                return new ResponseEntity<>(productRepo.save(productFound), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>("Can not found this product", HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>("Invalid file format!!", HttpStatus.BAD_REQUEST);
        }
    }

    //Delete: Chuyển từ Active sang Inactive
    @PutMapping("/products/delete/{id}")
    public ResponseEntity<?> inactiveProduct(@PathVariable("id") Long id) {
        try {
            Optional<Product> productCheck = productRepo.findById(id);
            if(productCheck.isPresent()){
                Product productFound = productCheck.get();

                productFound.setStatus("Inactive");

                return new ResponseEntity<>(productRepo.save(productFound), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>("Can not found this product", HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>("Invalid file format!!", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value="/products/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable("id") Long id) {
        try {  
        productRepo.deleteById(id);
        return new ResponseEntity<>("Xóa thành công", HttpStatus.OK);            
        } 
        catch (Exception e) { // Xử lý trả kết quả lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
