package com.laptop.backend.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.laptop.backend.entites.User;
import com.laptop.backend.repository.UserRepository;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    UserRepository userRepo;

    @GetMapping("/users")
    public ResponseEntity<?> getAllUsers(){
        try {
            List<User> users = new ArrayList<>();

            userRepo.findAll().forEach(users::add);

            return new ResponseEntity<>(users, HttpStatus.OK);
        } 
        catch (Exception e) { // Xử lý trả kết quả lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<?> getUserDetail(@PathVariable("id") Long id){
        try {
            Optional<User> userFound = userRepo.findById(id);
            if(userFound.isPresent()){
                return new ResponseEntity<>(userFound, HttpStatus.OK);
            }
            else {
                return ResponseEntity.unprocessableEntity().body("Cant not find this user" );
            }
        } 
        catch (Exception e) { // Xử lý trả kết quả lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/login/{username}/{password}")
    public ResponseEntity<?> loginUser(@PathVariable("username") String username, @PathVariable("password") String password ){
        try {
            User userFound  = userRepo.findByUserNameAndPassword(username, password);
            if(userFound != null){
                return new ResponseEntity<>(userFound, HttpStatus.OK);
            }
            else {
                return ResponseEntity.unprocessableEntity()
                .body("Cannot find this user" );
            }
        } 
        catch (Exception e) { // Xử lý trả kết quả lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/users")
    public ResponseEntity<Object> createUser(@RequestBody User pUser) {
        try {
            User newUser = new User();
         
            newUser.setUserName(pUser.getUserName());
            newUser.setPassword(pUser.getPassword());
            newUser.setEmail(pUser.getEmail());
            newUser.setFullName(pUser.getFullName());
            newUser.setPhoneNumber(pUser.getPhoneNumber());
            newUser.setAddress(pUser.getAddress());
            newUser.setAdmin(pUser.isAdmin());
            return new ResponseEntity<>(userRepo.save(newUser),HttpStatus.CREATED);
            
        } catch (Exception e) {
            System.out.println(e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to create User" );
        }
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<?> updateUser(@RequestBody User pUser, @PathVariable("id") Long id) {
        try {
            Optional<User> userCheck  = userRepo.findById(id);
            if(userCheck.isPresent()){
                User userFound = userCheck.get();
                userFound.setUserName(pUser.getUserName());
                userFound.setPassword(pUser.getPassword());
                userFound.setEmail(pUser.getEmail());
                userFound.setFullName(pUser.getFullName());
                userFound.setPhoneNumber(pUser.getPhoneNumber());
                userFound.setAddress(pUser.getAddress());
                userFound.setAdmin(pUser.isAdmin());

                return new ResponseEntity<>(userRepo.save(userFound), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>("Can not found this product", HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>("Invalid file format!!", HttpStatus.BAD_REQUEST);
        }
    }




}
