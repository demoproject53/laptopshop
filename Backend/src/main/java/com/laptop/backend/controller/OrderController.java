package com.laptop.backend.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.laptop.backend.entites.Order;
import com.laptop.backend.entites.User;
import com.laptop.backend.repository.OrderRepository;
import com.laptop.backend.repository.UserRepository;




@RestController
@CrossOrigin
public class OrderController {
    
    @Autowired
    OrderRepository orderRepo;

    @Autowired
    UserRepository userRepo;

    @GetMapping("/orders")
    public ResponseEntity<?> getOrders() {
        try {
            List<Order> allOrders = new ArrayList<>();

            orderRepo.findAll().forEach(allOrders::add);

            return new ResponseEntity<>(allOrders, HttpStatus.OK);
        } 
        catch (Exception e) { // Xử lý trả kết quả lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<?> getOrderbyId(@PathVariable("id") long id) {
        try {
            Optional<Order> orderFound = orderRepo.findById(id);
            if(orderFound.isPresent()){
                return new ResponseEntity<>(orderFound.get(), HttpStatus.OK);
            }
            else {
                return ResponseEntity.unprocessableEntity().body("Cant find this order" );
            }
        } 
        catch (Exception e) { // Xử lý trả kết quả lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/orders/code/{orderCode}")
    public ResponseEntity<?> getOrderbyCode(@PathVariable("orderCode") String code) {
        try {
            Optional<Order> orderFound = orderRepo.findByOrderCode(code);
            if(orderFound.isPresent()){
                return new ResponseEntity<>(orderFound.get(), HttpStatus.OK);
            }
            else {
                return ResponseEntity.unprocessableEntity().body("Cant find this order" );
            }
        } 
        catch (Exception e) { // Xử lý trả kết quả lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping("/orders/create/{userid}") 
    public ResponseEntity<Object> createOrder(@RequestBody Order pOrder, @PathVariable("userid") Long id) {
        try {
            Order newOrder = new Order();
         
            newOrder.setAddress(pOrder.getAddress());
            newOrder.setEmail(pOrder.getEmail());
            newOrder.setTotalPrice(pOrder.getTotalPrice());
            newOrder.setName(pOrder.getName());
            newOrder.setContact(pOrder.getContact());
            newOrder.setCreateDate(new Date());
            newOrder.setStatus("Open");
            
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            String orderCode = String.format("ORDER - %s", timestamp.getTime());
            newOrder.setOrderCode(orderCode);

            Optional <User> userFound = userRepo.findById(id);
            if(userFound.isPresent()){
                newOrder.setUser(userFound.get());
            } else {
                newOrder.setUser(null);
            }
 
            return new ResponseEntity<>(orderRepo.save(newOrder), HttpStatus.CREATED);
            
        } catch (Exception e) {
            System.out.println(e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create New Order" );
        }
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity<?> deleteOrder(@PathVariable("id") Long id) {
        try {
            orderRepo.deleteById(id);
            return new ResponseEntity<>("Xóa đơn hàng thành công", HttpStatus.OK);
        } 
        catch (Exception e) { // Xử lý trả kết quả lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/orders/{orderCode}/{status}") 
    public ResponseEntity<Object> updateOrder(@PathVariable("orderCode") String code, @PathVariable("status") String status ) {
        try {
            Optional<Order> orderCheck = orderRepo.findByOrderCode(code);
            if(orderCheck.isPresent()){
                Order orderFound = orderCheck.get();
                orderFound.setStatus(status);
                return new ResponseEntity<>(orderRepo.save(orderFound), HttpStatus.OK);
            }
            else {
                return ResponseEntity.unprocessableEntity().body("Cant find this order" );
            }
        } 
        catch (Exception e) { 
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
   

}
