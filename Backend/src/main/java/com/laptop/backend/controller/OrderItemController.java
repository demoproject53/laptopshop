package com.laptop.backend.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.laptop.backend.entites.Order;
import com.laptop.backend.entites.OrderItem;
import com.laptop.backend.entites.Product;
import com.laptop.backend.repository.OrderItemRepository;
import com.laptop.backend.repository.OrderRepository;
import com.laptop.backend.repository.ProductRepository;


@RestController
@CrossOrigin
public class OrderItemController {
    
    @Autowired
    OrderItemRepository orderItemRepo;

    @Autowired
    ProductRepository productRepo;

    @Autowired
    OrderRepository orderRepo;


    @GetMapping("/orderItems")
    public ResponseEntity<?> getOrders() {
        try {
            List<OrderItem> allOrderItems = new ArrayList<>();

            orderItemRepo.findAll().forEach(allOrderItems::add);

            return new ResponseEntity<>(allOrderItems, HttpStatus.OK);
        } 
        catch (Exception e) { // Xử lý trả kết quả lỗi
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/orderItems") 
    public ResponseEntity<Object> createCVoucher(@RequestBody List<OrderItem> listOrderItem, 
                        @RequestParam("order_id") Long orderId ) {
        try {
            Optional <Order> orderCheck = orderRepo.findById(orderId);
            Order orderFound = null;
            if(orderCheck.isPresent()){
                orderFound = orderCheck.get();
            } 

            ArrayList<OrderItem> orderItemCreated = new ArrayList<>();

            for (OrderItem orderItem : listOrderItem) {
                OrderItem newOrderItem = new OrderItem();

                newOrderItem.setName(orderItem.getName());
                newOrderItem.setPrice(orderItem.getPrice());
                newOrderItem.setQty(orderItem.getQty());
                newOrderItem.setPhoto(orderItem.getPhoto());

                Optional <Product> productCheck = productRepo.findById(orderItem.getProductId());
                Product productFound = null;
                if(productCheck.isPresent()){
                    productFound = productCheck.get();
                } 
                newOrderItem.setProduct(productFound);
                newOrderItem.setOrder(orderFound);

                orderItemCreated.add(newOrderItem);
            }
  
            return new ResponseEntity<>(orderItemRepo.saveAll(orderItemCreated), HttpStatus.CREATED);
            
        } catch (Exception e) {
            System.out.println(e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create Order Item List!" );
        }
    }

}
